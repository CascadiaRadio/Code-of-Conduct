# Cascadia Radio Administrative Team

Below is the current list of individuals that make up the Cascadia Radio Administrative Team. Please reach out to any or all of these individuals to report an issue with any Cascadia Radio property (website, GitLab repositories, Discord server, etc.) or to report a [Code of Conduct](https://gitlab.com/CascadiaRadio/Code-of-Conduct/-/blob/main/README.md) violation.

Unless you have specific reason not to, please include all active admins in any new Discord (group) direct messages or emails.

| Full Name            | Discord Username  | Email                  | Admin Since Date |
|----------------------|-------------------|------------------------|------------------|
| Ian Gallagher        | `cdine`           | ke7map@cdine.org       | 2017-11-27       |
| Ethan Schoonover     | `ethanschoonover` | es@ethanschoonover.com | 2017-11-27       |

# Cascadia Radio Squad

In addition to our Administrative Team, We maintain a rotating group of Cascadia Radio members that help actively grow and guide the community. Responsibilities include everything from server management to content creation and running events.

It's a real job and we thank all Squad members past and present for their contributions!

## Current Squad Members

- Ian - KE7MAP
- Ethan - W7ZOO
- Benjamin - WY2K
- Colin - K0CMT
- Tim - N7KOM

## Past Squad Members

- Quentin - K7DRQ
- Jessica - N4LKZ
