# Welcome to the Cascadia Radio Community.

## Introduction

Cascadia Radio is a free, online community of Pacific Northwest amateur radio operators. Our public website is https://www.cascadiaradio.org

Cascadia Radio was created out of a desire to help organize regional amateur radio events and stay in touch with people in a modern way. Cascadia Radio does not intend to be a traditional Amateur Radio Club, nor replace the many excellent and established clubs in the area.

We hope that you find the community to be enriching to your personal and professional lives, and above all else a place of camaraderie where our collective knowledge and differences are celebrated in a positive, welcoming environment.

Thank you for following this code of conduct. We reserve the right to amend or change the code of conduct at any time and encourage you to periodically review these guidelines to ensure a safe environment for all.

—[Cascadia Radio Administrative Team](https://gitlab.com/CascadiaRadio/Code-of-Conduct/-/blob/main/Admins.md)

---

## Code Of Conduct

All are welcome to participate in the Cascadia Radio Community (including the Discord server) as long as all tenets of this Code of Conduct are adhered to.

## Short Version

**All community members are expected to:**

- Respect differences in people, their ideas and opinions
- Treat one another with dignity and respect
- Respect and treat others fairly, regardless of race ancestry, place of origin, colour, ethnic origin, citizenship, religion, gender, sexual orientation, age or disability
- Respect the rights of others

This community is focused on _amateur radio and directly related topics_. We ask that members use this space to discuss these subjects and _avoid unrelated or potentially divisive topics_ including, for example, politics, or other subjects likely to derail discussions related to amateur radio.

**Additionally, if you are a reporter or a member of the press:**

- You are welcome here. Note that if you want to interview someone about something, you need to ask permission to interview them and inform them about where their comments may end up being published prior to the start of the interview. Practice journalistic informed consent.

**Not in the PNW? Not yet a ham? You too are welcome.**

Everyone is welcome to participate, including those who are not licensed amateur radio operators, and those outside of the Cascadia Bioregion / Pacific Northwest. No judgement will be made on members of the greater Amateur radio community who are not a part of the Cascadia Radio community.

## Long Version

### Policies

The Cascadia Radio Administrative Team is dedicated to providing a harassment-free experience for everyone, regardless of (but not limited to): gender, gender identity and expression, sexual orientation, disability, physical appearance, body size, race, age, religion, or preferred technological ecosystem.

We do not tolerate harassment of Discord participants in any form. Sexual language and imagery is not appropriate for any venue. Discord participants violating these rules may be sanctioned or expelled from the Cascadia Radio Discord Server at the discretion of the Administrative Team.

Harassment includes, but is not limited to:

- Offensive and/or unwelcome comments related to gender, gender identity and expression, sexual orientation, disability, physical appearance, body size, race, age, religion, or preferred technological ecosystem.
- Sexual images in public spaces
- Deliberate intimidation, stalking, or following
- Harassing photography or recording
- Sustained disruption of talks or other events
- Physical contact and simulated physical contact (e.g. textual descriptions like “hug” or “backrub”) without consent or after a request to stop
- Unwelcome sexual attention including gratuitous or off-topic sexual images or behaviour
- Hate speech
- Purposeful psychological manipulation of any kind
- Trolling, insulting/derogatory comments, and personal or political attacks
- Spamming of GIFs
- Advocating for, or encouraging, any of the above behaviour
- Direct messages or postings in channels/private groups of an overly sales/advertising/spam manner

A note on politics: Political discussions are not banned, but we don't tolerate using Discord as a personal soapbox to air political opinions. As always, the Cascadia Radio Administrative team has the final say on this.

### Enforcement

Participants asked to stop any harassing behavior are expected to comply immediately.

If a participant engages in harassing behaviour, the Administrative Team retains the right to take any actions to keep the community a welcoming environment for all participants, including warning the offender or expulsion of the offender from the Cascadia Radio Discord Server.

The Administrative Team may take action to redress anything designed for, or with the clear impact of, disrupting the community or making the environment hostile for any participants.

We expect participants to follow these rules in all public and private channels (including direct messages).

### Reporting

If you are being harassed, notice that someone else is being harassed, or have any other concerns, and you feel comfortable speaking with the offender, please inform the offender that they have affected you negatively. Often, the offending behavior is unintentional, and the accidental offender and offended will resolve the incident by having that initial discussion. As a public community, we recognize that there can be inherent language barriers we must work together to overcome.

The Cascadia Radio Administrative Team recognizes that there are many reasons speaking directly to the offender may not be workable for you (all reasons are valid, we will never ask you to explain nor defend your reasons). If you don't feel comfortable speaking directly with the offender _for any reason_, please report violations directly to a member of the Administrative Team.

[Current List of Cascadia Radio Administrative Team Members](https://gitlab.com/CascadiaRadio/Code-of-Conduct/-/blob/main/Admins.md).

The Administrative Team will handle all reports with discretion.

If a party to a dispute is also a member of the Administrative Team, that person may have no role in the dispute resolution process except as a party; forfeiting any involvement in the enforcement and resolution process. It is the responsibility of that person to immediately inform the Administrative Team of the conflict. The only way a conflict of interest is applicable is when a person involved in the mediation/resolution process is also involved in the conflict. As members, we are entrusted to make decisions in the best interest of the community and set aside personal interest for the best interest of our community as a whole.

If you have concerns regarding this Cascadia Radio or its Discord community, please feel free to contact the Administrative Team and we will work to understand and address them respectfully and with an appropriate level of privacy in the given situation.

### Contributions and Modifications

Anyone may recommend modifications to this Code of Conduct by opening a merge request [here](https://gitlab.com/CascadiaRadio/Code-of-Conduct/). The request will be reviewed by the Administrative Team before being approved and merged.

### Attribution

This Code of Conduct was based on the Code of Conduct documents from [The Mac Admins Slack Team](https://github.com/macadminsdotorg/codeofconduct) and [The Cuvva Community](https://github.com/cuvvacommunity/Code-of-Conduct/). Portions of the [Contributor Covenant 2.0](https://www.contributor-covenant.org/version/2/0/code_of_conduct/) have also been incorporated.
